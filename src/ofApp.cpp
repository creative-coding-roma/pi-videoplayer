#include "ofApp.h"
#include <thread>

void ofApp::receiveOrder(){
    while (receive.hasWaitingMessages()){
        ofxOscMessage m;
        receive.getNextMessage(&m);
        cout<< "## OSC: " << m.getAddress() << endl; 
        if (m.getAddress().compare("/exebox") != 0 ){
            loadNewVideo(m.getArgAsString(0));
        }
    }
}

void ofApp::loadNewVideo(const string& filename){
    std::thread t([this, filename]{
        ofVideoPlayer *target;
        if (display == &slotA){
            cout << "Using slotB" << endl;
            target = &slotB;
        } else {
            cout << "Using &slotA" << endl;

            target = &slotA;
        }

        target->load(filename);
        if (!target->isLoaded()){
            cout << "Error loading file :" << filename << endl;
            return;
        } 
        cout << "Loaded new video: " << filename << endl;
        target->play();
        std::lock_guard<std::mutex> lock(this->loader_mtx);
        display = target;

    });
    t.detach();
}

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetVerticalSync(true);
    receive.setup(PORT);

	slotA.load(filename);
    if (!slotA.isLoaded()){
        cout << "Error loading file :" << filename << endl;
        ofExit();
    }

	slotA.setLoopState(OF_LOOP_NORMAL);
    slotB.setLoopState(OF_LOOP_NORMAL);
	slotA.play();
    slotB.play();
    display = &slotA;
}

//--------------------------------------------------------------
void ofApp::update(){
    receiveOrder();
    
    {
    std::lock_guard<std::mutex> lock(loader_mtx);
    display->update();
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    std::lock_guard<std::mutex> lock(loader_mtx);
    display->draw(0,0, ofGetWidth(), ofGetHeight());
    
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    loadNewVideo("movies/fingers.mov");

}


//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
