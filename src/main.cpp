#include "ofMain.h"
#include "ofApp.h"

//========================================================================

int main(int argc, char const *argv[]){

	ofSetupOpenGL(1024,768, OF_WINDOW);			// <-------- setup the GL context

	if (argc != 2){
		std::cout << "Usage: videoPlayer filename" << std::endl;
		return 1;
	}

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new ofApp(argv[1]));

}
