#pragma once
#include <mutex> 
#include "ofMain.h"
#include "ofxOsc.h"
#define PORT 6767

class ofApp : public ofBaseApp{

	public:
		ofApp(const string& n): filename(n), ofBaseApp() {}
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void receiveOrder();
		void loadNewVideo(const string& );

		string filename;		
		std::mutex loader_mtx;	

		ofVideoPlayer 		slotA, slotB, *display;
		ofxOscReceiver receive;
};

