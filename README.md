# videoPlayer

Receives remote commands through OSC and plays the corresponding video (if exists).

Mostly useful when working on low-resources computers like the raspberry-pi

Example of resources usage when displaying a 640x480 h264 video

![htop](videoplayer_resources.png)

## Usage

Put inside the `apps` folder on openframeworks, and run
```$ make -j#'```
(where _#_ is the number of cores on the host processor, use 4 for the rPi3)

Launch the app with a starting file:
```$ bin/videoplayer path/to/video ```

Send Osc messages like
``` /exebox <s> (path/to/video)```

(the path can be relative to the `bin/data` folder)

## Known Issues

`.mp4` files are not working
